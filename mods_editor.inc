<?php
//$ Id: $

function mods_editor_settings() {
$form['misc']=array(
'#title'=>t('Miscellaneous'),
'#type'=>'fieldset',
'#collapsed'=>FALSE,
'#collapsible'=>TRUE,
);

$form['misc']['mod_editor_collapse_links_fieldset']=array(
'#title'=>t('Collapse Module Fieldsets on List Page'),
'#type'=>'checkbox',
'#default_value'=>variable_get('mod_editor_collapse_links_fieldset',FALSE),
);

return system_settings_form($form);
}
