<?php
//$ Id: $

function mods_editor_js() {
$output = _mods_editor_check_syntax($_POST['mods_editor_test_code']);
$status = (!$output[0] ? 'error' : 'status');
drupal_set_message($output[1]."<br>".$output[2], $status);
//drupal_json(array('status' => TRUE, 'data' => $output));
drupal_json(array('status' => TRUE, 'data' => theme_status_messages()));
}

function _mods_editor_check_syntax($code) {
$tmp_name = tempnam(conf_path() . "/files/tmp", "mod_editor_SYNCHCK");

$handle = fopen($tmp_name, "w");
fwrite($handle, $code);
fclose($handle);

$cmd = PHP_CMD . " -l ".$tmp_name;
exec($cmd,$output,$result);
$len = count($output);
if ( $len <= 0 ) {
	$title = "Sorry! internal error, no syntax check output :-(";
}

elseif ( substr($output[0],0,strlen(OK_LINE_START)) == OK_LINE_START ) {
	$syntax_OK = true;
	$title = "PHP syntax check: $hname is OK :-)";
} else {
	$syntax_OK = false;
	$title = "PHP syntax check: $hname FAILED :-(";
	$filtered_output = array();
	$err_line_nos = array();
	if ( $len > 0 && rtrim($output[0]) == "<br />" )  {
		array_shift($output);
		$len--;
	}
	if ( $len > 0 && rtrim($output[$len-1]) == "Errors parsing " . $tmp_name )  {
		$len--;   // N.B. skip last line
	}
	for ( $i=0; $i < $len; $i++ ) {
		$line = $output[$i];
		$filtered_output[] = str_replace($tmp_name,'Test File',$line);
		if ( preg_match(ON_LINE_PATTERN, $line, $matches) ) {
			$err_line_nos[] = $matches[1];
		}
	}
	sort($err_line_nos);
	$err_line_ct = count($err_line_nos);
}
$contents = file($tmp_name);
unlink($tmp_name);
if ( $syntax_OK ) {
	$out = t("No Errors Found");
} else {
	$out = mods_editor_error_list($filtered_output);
	$out .= "<br />" . mods_editor_source_list($contents, $err_line_nos);
}
return array($syntax_OK,$title,$out);
}

function mods_editor_error_list($filtered_output) {
	$i=0;
	foreach ( $filtered_output as $line ) {
		$i++;
		$out .= $line ."\n";
	}
	return $out;
}

function mods_editor_source_list($contents, $err_line_nos) {
	$i=0;
	foreach ( $contents as $line ) {
		$i++;
		$line_nos = str_pad($i,3," ",STR_PAD_LEFT);
		$numbered_line = $line_nos . ": " . htmlentities(rtrim($line));
		if ( in_array($i,$err_line_nos) ) {
			$out .=  '<span style="color:red; font-weight:bold;">' . t($numbered_line) ."</span><br />\n";
		}
		if(!$start){
		$out .= "<p>";
		$start = TRUE;
		}
		if( in_array(($i+2),$err_line_nos) ){

		$out .= t($numbered_line). "<br />";
		}
		if( in_array(($i+1),$err_line_nos) ){
		$out .= t($numbered_line). "<br />";
		}
		if( in_array(($i-1),$err_line_nos) ){
		$out .= t($numbered_line). "<br />";
		}
		if( in_array(($i-2),$err_line_nos) ){
		$out .= t($numbered_line);
			if($start){
		$out .= "</p>";
		$start = TRUE;
		}
		}

	}
			if($start){
		$out .= "</p>";
		$start = TRUE;
		}
	return $out;
}

function mods_editor_check_php() {
$cmd = PHP_CMD . " -l ".$_SERVER['DOCUMENT_ROOT'].'/'.drupal_get_path('module', 'mods_editor')."/syntax_test_good.php";
exec($cmd,$output,$result);
$len = count($output);

if ( $len <= 0 ) {
	$test['good']['ok'] = FALSE;
	$test['good']['status'] = 'The PHP command line did not return anything. Verify the path to your servers version of the PHP CLI';

}

elseif ( substr($output[0],0,strlen(OK_LINE_START)) == OK_LINE_START ) {
	$test['good']['ok'] = TRUE;
	$test['good']['status'] = 'The PHP command line seems to be working correctly.';
} 
else {
	$test['good']['ok'] = FALSE;
	$test['good']['status'] = 'The PHP command line has either returned an error on the syntax_test_good.php file or did not recognize the string provided as the OK Line Start pattern.';
}
foreach((array)$output as $outp) {
if($br){$out2.="<br />";};
$out1 .= $outp;
$br=TRUE;
}

    $test['good']['output'] = $out1;
$cmd2 = PHP_CMD . " -l ".$_SERVER['DOCUMENT_ROOT'].'/'.drupal_get_path('module', 'mods_editor')."/syntax_test_bad.php";
exec($cmd2,$output2,$result2);
$len2 = count($output2);
if ( $len2 <= 0 ) {
	$test['bad']['ok'] = FALSE;
	$test['bad']['status'] = 'The PHP command line did not return anything. Verify the path to your servers version of the PHP CLI';

}

elseif ( substr($output2[0],0,strlen(OK_LINE_START)) == OK_LINE_START ) {
	$test['bad']['ok'] = FALSE;
	$test['bad']['status'] = 'The PHP command line did not register an error for the syntax_test_bad.php file and should have, please check your servers configuration.';
} 
else {

	
	$filtered_output = array();
	$err_line_nos = array();
	if ( $len > 0 && rtrim($output2[0]) == "<br />" )  {
		array_shift($output2);
		$len--;
	}
	if ( $len > 0 && rtrim($output2[$len-1]) == "Errors parsing " . $tmp_name )  {
		$len--;   // N.B. skip last line
	}
	for ( $i=0; $i < $len; $i++ ) {
		$line = $output2[$i];
		$filtered_output[] = str_replace($tmp_name,'Test File',$line);
		if ( preg_match(ON_LINE_PATTERN, $line, $matches) ) {
			$err_line_nos[] = $matches[1];
		}
		
	}
	sort($err_line_nos);
	$err_line_ct = count($err_line_nos);
	if($err_line_ct === FALSE) {
	$test['bad']['ok'] = FALSE;
		$test['bad']['status'] ='The PHP CLI has not recognized the ON LINE PATTERN in the error output or returned an error but did not recognize the string provided as the OK Line Start pattern. Please check your settings and verify it against the out putedresults below.'.print_r($err_line_nos,1); 
		}
	else{
	$test['bad']['ok'] = TRUE;
	$test['bad']['status'] = 'The PHP command line has found an error in the syntax_test_bad.php.';
	}
}

$br = false; //reset the <br /> indicator
foreach((array)$output2 as $outp) {
if($br){$out2.="<br />";};
$out2 .= $outp;
$br=TRUE;

}
    $test['bad']['output'] = $out2;
if($test['good']['ok'] == TRUE && $test['bad']['ok']) {
$ok=TRUE;

}
else{
$ok=FALSE;
}
$status = '<div><h5>syntax_test_good.php</h5><p>'.$test['good']['status'].'</p><p><strong>Command Line Output</strong><br />'.$test['good']['output'].'</p></div>'.'<div><h5>syntax_test_bad.php</h5><p>'.$test['bad']['status'].'</p><p><strong>Command Line Output</strong><br />'.$test['bad']['output'].'</p></div>';
drupal_json(array('status' => TRUE, 'data' => $status, 'ok'=>$ok));
exit;
}